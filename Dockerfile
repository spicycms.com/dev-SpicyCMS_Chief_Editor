FROM python:2.7
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /var/www/run
RUN mkdir -p /var/www/app
RUN mkdir -p /var/www/env
RUN mkdir -p /var/www/media
RUN mkdir -p /var/www/siteskin
RUN mkdir -p /var/www/static
RUN mkdir -p /var/www/nginx.conf.d
RUN mkdir -p /var/www/log

WORKDIR /var/www/app

#ARG VIRTUAL_HOST

#ARG DB_HOST
#ARG DB_BACKEND
#ARG DB_NAME
#ARG DB_PASS
#ARG CREATE_NEW_DB
#ARG INIT_TEST_DATA

RUN apt-get update &&  \
    apt-get install -y \
    supervisor         \          
    openssh-client     \      
    postgresql-client  \
    emacs24-nox        \
    uwsgi              \
    && pip install     \
    uwsgi

ADD ./SpicyCMS_Chief_Editor/requirements.txt /var/www/app/requirements.txt
RUN pip install -r requirements.txt

ADD ./SpicyCMS_Chief_Editor /var/www/app
RUN mkdir -p /var/www/app/siteskins/current/static

#RUN ln -s /var/www/app/nginx-app.conf /var/www/nginx.conf.d/${VIRTUAL_HOST}.conf                                                                                                                                                       
#run ln -s /var/www/app/supervisor-app.conf /etc/supervisor/conf.d/

RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi
RUN chown -R uwsgi /var/www/app
#COPY uwsgi.sh /uwsgi.sh
#RUN chmod +x /uwsgi.sh

#VOLUME ["/var/www/run", "/var/www/app", "/var/www/env", '/var/www/media', '/var/www/static', '/var/www/log', '/var/www/siteskin', '/var/www/nginx.conf.d']

ADD ./docker-entrypoint.sh /docker-entrypoint.sh
CMD ["/docker-entrypoint.sh"]
#CMD ["supervisord", "-n"]

EXPOSE 80