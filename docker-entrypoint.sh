#!/bin/bash

if [ -z ${DB_BACKEND+x} ]; then
    export DB_BACKEND="django.db.backends.sqlite3"
fi

if [ -z ${DB_HOST+x} ]; then
    export DB_HOST="db"
fi


## works only with postgresql
if [ -z ${DB_USER+x} ]; then
    export DB_USER="postgres"
fi

if [ -z ${DB_PASS+x} ]; then
    export DB_PASS=""
    export DB_PASS_STR="-w" 
else
    export DB_PASS_STR="-W "$DB_PASS 
fi

##

if [ -z ${CREATE_NEW_DB+x} ]; then
    CREATE_NEW_DB=0
fi

if [ -z ${INIT_TEST_DATA+x} ]; then
    INIT_TEST_DATA=0
fi

if [ -z ${INIT_TEST_DATA+x} ]; then
    INIT_TEST_DATA=0
fi

if [ -z ${VIRTUAL_HOST+x} ]; then
    export VIRTUAL_HOST=$HOSTNAME
fi

if [ -z ${DB_NAME+x} ]; then
    export DB_NAME=$VIRTUAL_HOST
fi

echo "INFO: virtual_host= "$VIRTUAL_HOST
echo "INFO: database name: "$DB_NAME

#source /var/www/env/bin/activate

python <<'EOPYTHON'

import os, sys, time

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apps.settings')

from django.db import connection

try_number = 0
connected = False
while try_number < 10 and not connected:
	try:
	    connection.cursor()
	except:
	    print('Not connected to db, waiting...')
	else:
	    connected = True
	    print('Connected')
	time.sleep(1)
	try_number += 1

if not connected:
	sys.exit(1)

EOPYTHON

function init_database {
    echo "INFO: init database "${DB_BACKEND}

    echo no | python manage.py syncdb
}

function init_test_data {
    echo "INFO: init test data"
    # test data

}

function migrate_db {
    echo "INFO: "${DB_BACKEND}
    echo "WARNING: database exists, do nothing. TODO -- migrate or syndb + patch"
}

# SQLLite3
if [ $DB_BACKEND == "django.db.backends.sqlite3" ]; then
    if [ -f $DB_NAME ]; then
		if [ $CREATE_NEW_DB == 1 ]; then
		    rm $DB_NAME;
		    init_database;
		    
		else
		    migrate_db;
		    
		fi
    else
		init_database;
	
    fi

# PostgreSQL
elif [ $DB_BACKEND == "django.db.backends.postgresql_psycopg2" ]; then
    if [ $CREATE_NEW_DB == 1 ]; then
		psql -c "DROP DATABASE IF EXISTS \""${DB_NAME}"\";" -h $DB_HOST -U $DB_USER $DB_PASS_STR;
		psql -c "create database "\"${DB_NAME}"\";" -h $DB_HOST -U $DB_USER $DB_PASS_STR;
		init_database;

    elif psql -h $DB_HOST -U $DB_USER $DB_PASS_STR -lqt | cut -d \| -f 1 | grep -qw $DB_NAME; then
		migrate_db;

    else
		psql -c "create database "\"${DB_NAME}"\";" -h $DB_HOST -U $DB_USER $DB_PASS_STR;
		init_database;

    fi
    
	# MySQL
	#elif [ $DB_BACKEND == "django.db.backends.mysql" ]; then
	#    mysql -e 'create database IF NOT EXISTS '$DB_NAME' CHARACTER SET utf8 COLLATE utf8_general_ci;';
fi

if [ $INIT_TEST_DATA == 1 ]; then
    init_test_data;
fi


# TODO remove this if test_data contains superuser role admin/admin
python <<'EOPYTHON'

import os, sys

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apps.settings')

from spicy.core.profile import defaults
from spicy.utils.models import get_custom_model_class

from django.conf import settings
from django.contrib.sites.models import Site
from django.core import exceptions

Profile = get_custom_model_class(defaults.CUSTOM_USER_MODEL)

try:
	Profile.objects.get(is_superuser=True)
except Profile.DoesNotExist:
	print('No superuser found. Creating one...')
	sites = Site.objects.all()
	if not sites:
		site_url = getattr(settings, 'SITE_URL', 'example.com')
		site_name = getattr(settings, 'SITE_NAME', site_url)
		site = Site(domain=site_url, name=site_name)
		site.save()
		sites = Site.objects.all()

	profile = Profile.objects.create_inactive_user(email='admin@example.com', password='admin',
	                                  is_staff=True, send_email=False)

	profile.sites.add(*sites)
	profile.activate()
	profile.is_superuser = True
	profile.save()
	print "Superuser created successfully!\nLogin: admin\nPassword: admin\n"

EOPYTHON


python manage.py help

# update current theme templates, make them active in the empty database
python manage.py update-simplepages

python manage.py runserver 0.0.0.0:80
