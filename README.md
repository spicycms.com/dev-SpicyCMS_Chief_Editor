Branch | Django compatible version | build status | Coverage status | Docs | Demo link |
-------|---------------------------|--------------|-----------------|------|-----------|
master | 1.3? - 1.5.12 |  ![master build](https://gitlab.com/spicycms.com/dev-SpicyCMS_Chief_Editor/badges/master/build.svg) | ![master coverage](https://gitlab.com/spicycms.com/dev-SpicyCMS_Chief_Editor/badges/master/coverage.svg) | todo | todo |
develop | 1.3? - 1.5.12 | ![develop build](https://gitlab.com/spicycms.com/dev-SpicyCMS_Chief_Editor/badges/develop/build.svg) | ![develop coverage](https://gitlab.com/spicycms.com/dev-SpicyCMS_Chief_Editor/badges/develop/coverage.svg) | todo | todo |



### Краткая инструкция по установке

Конируем последнюю ветку репозитория для разработчиков ``develop``

```
git clone --recursive -b develop git@gitlab.com:spicycms.com/dev-SpicyCMS_Chief_Editor.git
cd dev-SpicyCMS_Chief_Editor
git submodule foreach --recursive 'git checkout develop' # Уходим с detached HEAD подмодулей
git submodule foreach --recursive 'git pull' # Убеждаемся, что у нас последние коммиты
docker-compose up
```

*Адрес: http://localhost/*


### Запуск проекта с virtualenv (для простых Django приложений)

```
virtualenv venv
source env/bin/activate
pip install -r ./SpicyCMS_Chief_Editor/requirements.txt
./SpicyCMS_Chief_Editor/manage.py runserver
```

*Адрес: http://localhost:8000/*

### Для работы с кодом приложений

* [Детальная инструкция по работе с конфигурационным репозиторием и подмодулями](https://gitlab.com/bramabrama/workflow/tree/master/dev-repositories)
* [Общее положение о стиле программирования и релизах](https://gitlab.com/bramabrama/workflow)

## Проектная документация SpicyCMS Chief Editor (CE)

* [Конфигурация Django приложения](https://gitlab.com/spicycms.com/cms.chiefeditor/)
* [Документация по работе темой](https://gitlab.com/spicycms.com/cms.chiefeditor/tree/develop/siteskins)
* [Документация пользователя](https://gitlab.com/spicycms.com/cms.chiefeditor/tree/develop/docs)


# Помощь

* [Slack](https://bramabrama.slack.com/messages/C258N9SAE)
* [Задачи и пожеления](https://gitlab.com/spicycms.com/cms.chiefeditor/issues)
 

# Индивидуальная разработка Django проектов

http://bramabrama.com
sales@bramabrama.com